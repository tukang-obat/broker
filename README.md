# Broker
- Create password file `password.txt` with the following format:
    ```
    $username:$password
    ```
- Run the container using:
    ```
    docker-compose up --build --detach
    ```
